package example.com.textswitcherexample;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

public class MainActivity extends AppCompatActivity {
    TextSwitcher messageText;
    Button NextButton;
    int index;
     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        messageText=(TextSwitcher)findViewById(R.id.messageText);
        NextButton=(Button)findViewById(R.id.switchButton);
         final String messageContent[]={"Text Switcher","Android application","Sample Text"};
         index=-1;
         final int messageCount=messageContent.length;

         messageText.setFactory(new ViewSwitcher.ViewFactory() {
             @Override
             public View makeView() {
                 TextView myText = new TextView(MainActivity.this);
                 myText.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                 myText.setTextSize(25);
                 myText.setTextColor(Color.GREEN);
                 return myText;
             }
         });
         Animation  in= AnimationUtils.loadAnimation(this,android.R.anim.slide_in_left);
         Animation out=AnimationUtils.loadAnimation(this,android.R.anim.slide_out_right);
         messageText.setAnimation(in);
         messageText.setAnimation(out);
        NextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index++;
                if(index==messageCount)
                    index=0;
                messageText.setText(messageContent[index]);

            }
        });
    }

}
