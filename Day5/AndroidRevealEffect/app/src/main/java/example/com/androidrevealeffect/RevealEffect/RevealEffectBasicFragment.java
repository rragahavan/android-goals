package example.com.androidrevealeffect.RevealEffect;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;

import example.com.androidrevealeffect.Common.Logger.Log;
import example.com.androidrevealeffect.R;

/**
 * Created by Administrator on 18-04-2016.
 */
public class RevealEffectBasicFragment extends Fragment {

    private final static String TAG = "RevealEffectBasicFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.reveal_effect_basic, container, false);

        View button = rootView.findViewById(R.id.button);

        // Set a listener to reveal the view when clicked.
        button.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                View shape = rootView.findViewById(R.id.circle);

                // Create a reveal {@link Animator} that starts clipping the view from
                // the top left corner until the whole view is covered.
                Animator animator = ViewAnimationUtils.createCircularReveal(
                        shape,
                        0,
                        0,
                        0,
                        (float) Math.hypot(shape.getWidth(), shape.getHeight()));

                // Set a natural ease-in/ease-out interpolator.
                animator.setInterpolator(new AccelerateDecelerateInterpolator());

                // Finally start the animation
                animator.start();

                Log.d(TAG, "Starting Reveal animation");
            }
        });

        return rootView;
    }

}
