package example.com.androidrevealeffect.Common.Logger;

/**
 * Created by Administrator on 18-04-2016.
 */
public interface LogNode {
    public void println(int priority, String tag, String msg, Throwable tr);
}
