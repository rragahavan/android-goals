package office.work.example.activityscenetransitionbasic;

/**
 * Created by Administrator on 16-03-2016.
 */
public class Item {
    //private static final String LARGE_BASE_URL = "http://i.telegraph.co.uk/multimedia/archive/03477/";
    private static final String LARGE_BASE_URL = "https://avatars.githubusercontent.com/u/";
    private static final String THUMB_BASE_URL = "https://avatars.githubusercontent.com/u/";

    public static Item[] ITEMS = new Item[] {
           // new Item("Viru", "Sehwag", "Virender_Sehwag_3477818b.jpg"),
            new Item("David", "", "1?v=3.png"),

            new Item("Jacob", "Romain Guy", "2?v=3.png"),
            new Item("Jhon", "Romain Guy", "3?v=3.png"),
            new Item("Fleming", "Romain Guy", "4?v=3.png"),
            new Item("ponting", "Romain Guy", "5?v=3.png"),
            new Item("Levi", "Romain Guy", "6?v=3.png"),
            new Item("Kevin", "Romain Guy", "7?v=3.png"),
            new Item("Jose", "Romain Guy", "8?v=3.png"),
    };

    public static Item getItem(int id) {
        for (Item item : ITEMS) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }

    private final String mName;
    private final String mAuthor;
    private final String mFileName;

    Item (String name, String author, String fileName) {
        mName = name;
        mAuthor = author;
        mFileName = fileName;
    }

    public int getId() {
        return mName.hashCode() + mFileName.hashCode();
    }

    public String getAuthor() {
        return mAuthor;
    }

    public String getName() {
        return mName;
    }

    public String getPhotoUrl() {
        return LARGE_BASE_URL + mFileName;
    }

    public String getThumbnailUrl() {
        return THUMB_BASE_URL + mFileName;
    }
}
