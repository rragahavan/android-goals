package office.work.example.activityscenetransitionbasic.Common.logger;

/**
 * Created by Administrator on 16-03-2016.
 */
public interface LogNode {
    public void println(int priority, String tag, String msg, Throwable tr);
}
