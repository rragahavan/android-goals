package works.goavega.com.weekilyassignment.Activities;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;

import works.goavega.com.weekilyassignment.Adapters.ViewPagerAdapter;
import works.goavega.com.weekilyassignment.R;
import works.goavega.com.weekilyassignment.Util.BaseActivity;

public class TrendsActivity extends BaseActivity {
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private DrawerLayout mDrawerLayout;
    ImageView menuIconImage;




    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trends);
        menuIconImage = (ImageView) findViewById(R.id.menuIcon);
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
        set(navMenuTitles, navMenuIcons);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        menuIconImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        viewPager=(ViewPager)findViewById(R.id.viewpager);
        tabLayout= (TabLayout) findViewById(R.id.tabs);
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
       // setSupportActionBar(toolbar);
        final TabLayout.Tab today = tabLayout.newTab();
        final TabLayout.Tab week = tabLayout.newTab();
        final TabLayout.Tab month = tabLayout.newTab();
        today.setText("TODAY");
        week.setText("WEEK");
        month.setText("MONTH");
        tabLayout.addTab(today, 0);
        tabLayout.addTab(week, 1);
        tabLayout.addTab(month, 2);
        tabLayout.setTabTextColors(ContextCompat.getColorStateList(this, R.color.tab_selector));
        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.indicator));


        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));








    }

}
