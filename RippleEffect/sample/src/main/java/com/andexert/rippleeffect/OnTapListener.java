package com.andexert.rippleeffect;

public interface OnTapListener
{
    public void onTapView(int position);
}
