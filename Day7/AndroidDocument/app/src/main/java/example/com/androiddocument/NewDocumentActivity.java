package example.com.androiddocument;

import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

public class NewDocumentActivity extends Activity {
    private TextView mDocumentCounterTextView;
    private int mDocumentCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_document);
        mDocumentCount = getIntent()
                .getIntExtra(MainActivity.KEY_EXTRA_NEW_DOCUMENT_COUNTER, 0);
        mDocumentCounterTextView = (TextView) findViewById(
                R.id.hello_new_document_text_view);
        setDocumentCounterText(R.string.hello_new_document_counter);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setDocumentCounterText(R.string.reusing_document_counter);
    }

    public void onRemoveFromOverview(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask();
        }
    }

    public void setDocumentCounterText(int resId) {
        mDocumentCounterTextView.setText(String.format(getString(resId), String.valueOf(mDocumentCount)));
    }
}
