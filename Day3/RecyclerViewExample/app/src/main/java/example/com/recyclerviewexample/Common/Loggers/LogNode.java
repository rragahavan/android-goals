package example.com.recyclerviewexample.Common.Loggers;

/**
 * Created by Administrator on 07-04-2016.
 */
public interface LogNode {
    public void println(int priority, String tag, String msg, Throwable tr);
}
